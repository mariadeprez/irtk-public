/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkPolyDataToTetrahedralGrid.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPolyDataToTetrahedralGrid - reates a tetrahedral mesh
// from the volume defined by a vtkPolyData.
// .SECTION Description
// vtkPolyDataToTetrahedralGrid is a class that creates a tetrahedral mesh
// from a vtkPolyData consisting of one or more watertight components. This
// class wraps the TetGen library. For more details on the algorithm, see
//
// Si, H. and Gaertner, K. (2005). Meshing piecewise linear complexes by 
// constrained Delaunay tetrahedralizations. In Proceedings of the 14th 
// International Meshing Roundtable, pages 147–163.
//
// .SECTION Warning
// As implemented, this class uses TetGen's default tetrahedralizer algorithm.
// This is fast but may not produce high-quality meshes. If you need 
// high-quality meshes, modify the flags passed in to the tetrahedralize()
// function. A list of options is available at
//
// http://tetgen.berlios.de/switches.html

//Modified on 1st March 2017 to generate unconstrained grids for Giulio

#ifndef __vtkPolyDataToUnconstrainedTetrahedralGrid_h
#define __vtkPolyDataToUnconstrainedTetrahedralGrid_h

//#include "vtkmsFilteringWin32Header.h"

#include "vtkUnstructuredGridAlgorithm.h"

class vtkDoubleArray;

//class VTK_MS_FILTERING_EXPORT vtkPolyDataToTetrahedralGrid : public vtkUnstructuredGridAlgorithm
class vtkPolyDataToUnconstrainedTetrahedralGrid : public vtkUnstructuredGridAlgorithm
{
public:
  static vtkPolyDataToUnconstrainedTetrahedralGrid *New();
  vtkTypeRevisionMacro(vtkPolyDataToUnconstrainedTetrahedralGrid,vtkUnstructuredGridAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
   
   vtkSetMacro(ControlCalidad,int);
   vtkGetMacro(ControlCalidad,int);
   vtkBooleanMacro(ControlCalidad,int);
   
   vtkSetMacro(ControlVolumen,int);
   vtkGetMacro(ControlVolumen,int);
   vtkBooleanMacro(ControlVolumen,int);
   
   vtkSetMacro(Calidad,float);
   vtkGetMacro(Calidad,float);
   
   vtkSetMacro(Volumen,float);
   vtkGetMacro(Volumen,float);


protected:
  vtkPolyDataToUnconstrainedTetrahedralGrid();
  ~vtkPolyDataToUnconstrainedTetrahedralGrid();

  // Description:
  // Fills in input data port information.
  virtual int FillInputPortInformation(int port, vtkInformation* info);

  // Description:
  // Usual data generation method.
  virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);
   
   int ControlCalidad;
   float Volumen;
   int ControlVolumen;
   float Calidad;

private:
  vtkPolyDataToUnconstrainedTetrahedralGrid(const vtkPolyDataToUnconstrainedTetrahedralGrid&);  // Not implemented.
  void operator=(const vtkPolyDataToUnconstrainedTetrahedralGrid&);  // Not implemented.
};

#endif
