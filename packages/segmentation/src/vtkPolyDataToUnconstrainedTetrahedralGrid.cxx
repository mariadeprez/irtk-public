/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkPolyDataToTetrahedralGrid.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPolyDataToUnconstrainedTetrahedralGrid.h"

#include "vtkCellArray.h"
#include "vtkDoubleArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkTetra.h"
#include "vtkTriangle.h"
#include "vtkUnstructuredGrid.h"

#include "tetgen1.4.2/tetgen.h"
#include "tetgen1.4.2/tetgen.cxx"
#include "tetgen1.4.2/predicates.cxx"
//#include "../tetgen1.4.3/tetgen.h"
//#include "../tetgen1.4.3aux/tetgen.cxx"
//#include "../tetgen1.4.3/predicates.cxx"
//#include "../tetgen1.4.3/tetgen.cxx"

#include <iostream>
#include <sstream>
#include <iomanip>


vtkCxxRevisionMacro(vtkPolyDataToUnconstrainedTetrahedralGrid, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkPolyDataToUnconstrainedTetrahedralGrid);

//----------------------------------------------------------------------------
vtkPolyDataToUnconstrainedTetrahedralGrid::vtkPolyDataToUnconstrainedTetrahedralGrid()
{
   
   this->ControlCalidad=0;
   this->ControlVolumen=0;
   this->Volumen=10.0;//100.0
   this->Calidad=1.414;
  // by default assume filters have one input and one output
  // subclasses that deviate should modify this setting
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------
vtkPolyDataToUnconstrainedTetrahedralGrid::~vtkPolyDataToUnconstrainedTetrahedralGrid()
{
}

//----------------------------------------------------------------------------
void vtkPolyDataToUnconstrainedTetrahedralGrid::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}


int vtkPolyDataToUnconstrainedTetrahedralGrid::FillInputPortInformation(int port, vtkInformation* info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPolyData");
  return 1;
}

//----------------------------------------------------------------------------
tetgenio* vtkPolyDataToUnconstrainedTetrahedralGridCreateTetgenio(vtkPolyData *polyData) {

   tetgenio *out = new tetgenio();
  
   // All indices start from 0.
   out->firstnumber = 0;

   out->numberofpoints = polyData->GetNumberOfPoints();
   out->pointlist = new REAL[out->numberofpoints * 3];
   double inputPoints[3];
   for (int i = 0; i < out->numberofpoints; i++) {
      polyData->GetPoint(i, inputPoints);
      out->pointlist[i*3 + 0] = inputPoints[0];
      out->pointlist[i*3 + 1] = inputPoints[1];
      out->pointlist[i*3 + 2] = inputPoints[2];
   }

   //out->numberoffacets = polyData->GetNumberOfPolys();
   //out->facetlist = new tetgenio::facet[out->numberoffacets];
   //out->facetmarkerlist = NULL;

   //vtkCellArray *polys = polyData->GetPolys();
   //int polyId = 0;
   //vtkIdType npts, *pts;
   //for (polys->InitTraversal(); polys->GetNextCell(npts, pts); ) {
   //   tetgenio::facet *f = &out->facetlist[polyId++];
   //   f->numberofpolygons = 1;
   //   f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
   //   f->numberofholes = 0;
   //   f->holelist = NULL;

   //   tetgenio::polygon *p = &f->polygonlist[0];
   //   p->numberofvertices = npts;
   //   p->vertexlist = new int[p->numberofvertices];
   //   for (int i = 0; i < npts; i++) {
   //	 p->vertexlist[i] = pts[i];
   //   }
   //}

  return out;
}

//----------------------------------------------------------------------------
// This is the superclasses style of Execute method.  Convert it into
// an imaging style Execute method.
int vtkPolyDataToUnconstrainedTetrahedralGrid::RequestData(
  vtkInformation* request,
  vtkInformationVector** inputVector,
  vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkUnstructuredGrid *output = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // Create tetrahedral mesh from input closed manifold
  tetgenio *in, *detect, out;

  in = vtkPolyDataToUnconstrainedTetrahedralGridCreateTetgenio(input);
   
  // First, detect if there are any overlapping polygons in the geometry
  //detect = new tetgenio();
  //detect->numberofpoints = 0;
  //try {
    //char options[] = "dQ";
    //tetrahedralize(options, in, detect);
  //} catch (...) {
    //vtkErrorMacro(<< "Error when testing geometry for overlapping polygons");
  //}

  //if (detect->numberofpoints == 0) { // Geometry passed the test?
    // This shouldn't be necessary, but it seems to prevent some
    // crashes when tetrahedralizing some geometries.
    //delete in;
    //in = vtkPolyDataToTetrahedralGridCreateTetgenio(input);

    try {       
       std::string optionsa="";//Non-CDT
       std::string optionsb="q";//Control de calidad
       std::stringstream sa;
       std::stringstream sb;
       sa.flags(std::ios::fixed); //write floating point values in fixed-point notation
       sa.precision(3); //indicar el numero de decimales a mostrar
       sa.width(0); //Especificar el ancho qeu va a ocupar todo el numero, Se rellena con espacios en blanco, Si ponemos un numero inferior a la longitud del numero, imprime al menos todos los numeros enteros mas los decimales indicados en precision.
       sa << this->Calidad;
       std::string optionsc=sa.str();
       std::string optionsd="a";
       sb.flags(std::ios::fixed); //write floating point values in fixed-point notation
       sb.precision(3); //indicar el numero de decimales a mostrar
       sb.width(0); //Especificar el ancho qeu va a ocupar todo el numero, Se rellena con espacios en blanco, Si ponemos un numero inferior a la longitud del numero, imprime al menos todos los numeros enteros mas los decimales indicados en precision.
       sb << this->Volumen;
       std::string optionse=sb.str();
       std::string optionsf="Q";
       std::string optionsg;
       if (this->ControlCalidad==0 && this->ControlVolumen==0)
	 optionsg=optionsa+optionsf;
       else if (this->ControlCalidad==0 && this->ControlVolumen==1)
	 optionsg=optionsa+optionsd+optionse+optionsf;
       else if (this->ControlCalidad==1 && this->ControlVolumen==0)
	 optionsg=optionsa+optionsb+optionsc+optionsf;
       else
	 optionsg=optionsa+optionsb+optionsc+optionsd+optionse+optionsf;

       char *optionsh;
       
       optionsh = new char[optionsg.length() + 1];
       strcpy(optionsh, optionsg.c_str());
       // now work with optionsf, which is a copy of optionse     
       // clean up
//       char optionsb[] = "pq1.414a5Q";
//       if (this->Calidad==0)
//	 tetrahedralize(optionsa, in, &out);
//       else
	 tetrahedralize(optionsh, in, &out);
       delete [] optionsh;
    } catch (...) {
      vtkErrorMacro(<< "Error when generating Delaunay tetrahedralization of geometry");

      // Initialize output data structure
      out.numberofpoints = 0;
      out.numberoftetrahedra = 0;
    }
  //} else {
  //  out.numberofpoints = 0;
  //  out.numberoftetrahedra = 0;
  //}
  //delete detect;
  delete in;

  vtkPoints *points = vtkPoints::New();
  points->SetNumberOfPoints(out.numberofpoints);
  for (int i = 0; i < out.numberofpoints; i++) {
    double *ptlist = i*3 + out.pointlist;
    points->SetPoint(i, i*3 + out.pointlist);
  }
   
  output->Allocate(out.numberoftetrahedra, 100);
  vtkTetra *tetra = vtkTetra::New();
  for (int i = 0; i < out.numberoftetrahedra; i++) {
    
    tetra->GetPointIds()->SetId(0, out.tetrahedronlist[4*i + 0]);
    tetra->GetPointIds()->SetId(1, out.tetrahedronlist[4*i + 1]);
    tetra->GetPointIds()->SetId(2, out.tetrahedronlist[4*i + 2]);
    tetra->GetPointIds()->SetId(3, out.tetrahedronlist[4*i + 3]);
    output->InsertNextCell(tetra->GetCellType(), tetra->GetPointIds());
    
  }

  tetra->Delete();
  output->SetPoints(points);
  points->Delete();

  return 1;
}
